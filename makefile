coursework:
    cl wordcount.c
clean:
	del *.obj
	del *.exe
	del *.asm
fullSentences:
	wordcount -i sentences_test.txt -o output.txt -c
fullSingleWords:
	wordcount -i single_words_test.txt -o output.txt -c
inputOutputSentences:
	wordcount -i sentences_test.txt -o output.txt
inputOutputSingleWord:
	wordcount -i single_words_test.txt -o output.txt
inputCSentences:
	wordcount -i sentences_test.txt -c
inputCSingleWords:
	wordcount -i single_words_test.txt -c
inputSentences:
	wordcount -i sentences_test.txt
inputSingleWords:
	wordcount -i single_words_test.txt
outputC:
	wordcount -o output.txt -c
output:
	wordcount -o output.txt
wordcountC:
	wordcount -c
wordcount:
	wordcount