Author: Venetsia Krasteva 
Matriculation number: 40313507
Tool chain for building - Notepad++ v7.5.4 (32-bit)
Running on: Visual Studio 2017 Tools Command Prompt (Developer Command Prompt for Vs 2017 (DEV))

Explaining how to use the wordcount application
	1) open Developer Command Prompt for Vs 2017 (DEV)
	2) type nmake coursework
	3) Choose one of the options and type it with nmake:
 
			fullSentences:
				wordcount -i sentences_test.txt -o output.txt -c
			fullSingleWords:
				wordcount -i single_words_test.txt -o output.txt -c
			inputOutputSentences:
				wordcount -i sentences_test.txt -o output.txt
			inputOutputSingleWord:
				wordcount -i single_words_test.txt -o output.txt
			inputCSentences:
				wordcount -i sentences_test.txt -c
			inputCSingleWords:
				wordcount -i single_words_test.txt -c
			inputSentences:
				wordcount -i sentences_test.txt
			inputSingleWords:
				wordcount -i single_words_test.txt
			outputC:
				wordcount -o output.txt -c
			output:
				wordcount -o output.txt
			wordcountC:
				wordcount -c
			wordcount:
				wordcount

		example: nmake fullSentences
	4) To delete all of the *.obj/ *.exe/ .*asm type nmake clean

A quick overview:
The wordcount includes stdio.h, ctype.h, string.h, stdlib.h and the manualy created wordOccurrences.c and count.h
It takes two files (*fi - file for input; *fo - file for output). If the user types from stdin the variable is *input which is
a dynamic memory location and it is freed in the end. If argc is 1 the settings are by default otherwise there are cases. If -i 
is found it sets iFlag to 1 and opens on i+1 (if -i is found open the next). Same goes for if -o is found. If -c is found it 
only sets cFlag to 1 for now.
After we exit the check for argc. The program starts to do things.
If iFlag is 1 it will do countForFile method and wordOccurrencesForFile method.
If not it will promp the user for input, afterwards if cFlag is 1 it loop through input find all the uppercases and conver them 
into lowercases. Also tab will be converted as a space. All punctuations will be converted to space as well. The double spacing
will be converted to a single space and then the word count will be done as well as the wordOccurrences. If cFlag isn't 1 it will
convert all tabs and double spaces to a single space. Wordcount is gonna be done and wordOccurrences as well. If oFlag is 1 the 
file opened will be closed at the end. *input and *ch will be cleaned.
